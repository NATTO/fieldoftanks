using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UniRx;
using System.Linq;

/// <summary>
/// 敵の移動ルートエディタ
/// </summary>
[CustomEditor(typeof(ScriptablePathTokenEnemy))]
public class PathTokenEnemyCustomEditor : Editor
{
    ScriptablePathTokenEnemy _PathTokenEnemy;
    private void OnEnable()
    {
        _PathTokenEnemy = target as ScriptablePathTokenEnemy;
        SceneView.onSceneGUIDelegate += OnSceneGUI;
    }
    void OnDisable()
    {
        SceneView.onSceneGUIDelegate -= OnSceneGUI;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

    }

    void OnSceneGUI(SceneView sceneView)
    {
        //移動する先のリストを取得
        List<TokenEnemy> tokens = _PathTokenEnemy._OnTheWayPositions.ToList();

        for (int index = 0; index < tokens.Count; index++)
        {

            Vector3 nodePosition = tokens[index]._Position;

            Vector3 newNodePosition = nodePosition;

            //ノードの位置にハンドルを表示
            newNodePosition = Handles.PositionHandle(newNodePosition, Quaternion.identity);
            Handles.color = Color.blue;

            //ノードの位置が変わったら
            if(newNodePosition != nodePosition)
            {
                //ノード登録して位置を更新
                Undo.RecordObject(target, "Moved Point");
                tokens[index]._Position = newNodePosition;
            }

            //ハンドル操作できるやつ
            Handles.SphereHandleCap(0, nodePosition, Quaternion.identity, 1f, EventType.Repaint);

            Handles.Label(nodePosition, $"Node{index}");
            //前のノードへ線延ばす
            if(index != 0)
                Handles.DrawLine(tokens[index]._Position, tokens[index-1]._Position);
        }
    }
}
