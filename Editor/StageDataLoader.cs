﻿using Cysharp.Text;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/// <summary>
/// 
/// </summary>
public class StageDataLoader : EditorWindow
{
    static EditorWindow _NowWindow = null;
    [MenuItem("Window/StageLoader")]
    public static void Window()
    {
        _NowWindow = EditorWindow.GetWindow(typeof(StageDataLoader));
    }

    List<AsyncOperation> descriptionSpriteAsyncOperation = new List<AsyncOperation>();
    TextAsset csvAsset;
    static readonly string _StageDataPath = /*Application.dataPath + "/" +*/ "Assets/" + "StageData{0}.asset";
    static readonly string _DescriptionSpritePath = "Assets/Image" + "/" + "StageDescriptionPicture";

    private void OnGUI()
    {
        csvAsset = EditorGUILayout.ObjectField("StageCSV", csvAsset, typeof(TextAsset), false) as TextAsset;
        if (GUILayout.Button("生成"))
        {
            if (csvAsset == null)
            {
                Debug.Log("csvが無い！");
                return;
            }

            Create();
        }
    }

    void Create()
    {
        string idFirst = "1";
        var data = new StageData();
        data.name = idFirst;
        AssetDatabase.CreateAsset(data, ZString.Format(_StageDataPath, idFirst));

        string[] stageStringData = csvAsset.text.Split('\n');
        data._StageID = new StageID[stageStringData.Length - 2];

        for (int i = 1; i < stageStringData.Length - 1; i++)
        {
            string[] splitData = stageStringData[i].Split(",");
            if (!splitData[0].StartsWith(idFirst))
            {
                idFirst = splitData[0].Substring(0, 1);
                EditorUtility.SetDirty(data);
                AssetDatabase.SaveAssets();
                data = new StageData();
                data.name = idFirst;
                AssetDatabase.CreateAsset(data, _StageDataPath + idFirst);
            }

            data._StageID[i - 1].ID = splitData[0];
            string descriptionSpritePath = _DescriptionSpritePath + "/" + splitData[1] + ".jpg";

            data._StageID[i - 1].DescriptionSprite = AssetDatabase.LoadAssetAtPath<Sprite>(descriptionSpritePath);
            data._StageID[i - 1].SceneName = splitData[2];
        }

        EditorUtility.SetDirty(data);
        AssetDatabase.SaveAssets();
    }
}