﻿using UnityEditor;
using UnityEngine;

/// <summary>
/// ステージ選択ボタンをテスト生成するやつ
/// </summary>
[CustomEditor(typeof(StageSelectUI))]
class StageSelectUICustomEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if(GUILayout.Button("テスト生成"))
        ((StageSelectUI)target).TestGenerate();
    }
}

