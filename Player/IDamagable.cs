﻿using UnityEngine;

interface IDamagable
{
    void TakeDamage(float damage, Vector3 damagePosition);
}