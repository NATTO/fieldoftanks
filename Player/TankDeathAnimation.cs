using Cysharp.Threading.Tasks;
using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Playables;
using Random = UnityEngine.Random;

public class TankDeathAnimation : MonoBehaviour, IStatusInitializable
{
    [SerializeField]
    Rigidbody _Rigidbody;
    [SerializeField]
    float _DeathExplotionPower;
    [SerializeField]
    AssetReferenceT<GameObject> _DeathParticleReference;
    [SerializeField]
    AssetReferenceT<GameObject> _DeathParticleAfterAnimationReference;
    [SerializeField]
    float _DeathAnimationTime;
    [SerializeField]
    PlayableDirector _Director;
    [SerializeField]
    PlayableAsset _SETimelineAsset;
    public float DeathAnimationTime => _DeathAnimationTime;
    ParticleSystem _DeathParticle;
    ParticleSystem _DeathParticleAfterAnimation;
    Subject<Unit> _AnimationStartSubject = new Subject<Unit>();
    public IObservable<Unit> AnimationStartObservable => _AnimationStartSubject;
    Subject<Unit> _AnimationEndSubject = new Subject<Unit>();
    public IObservable<Unit> AnimationEndObservable => _AnimationEndSubject;

    public async UniTask AnimationStart(Vector3 damagePosition)
    {
        Vector3 randomForce = new Vector3(Random.Range(-1, 1), 3, Random.Range(-1, 1)) * _DeathExplotionPower;
        Vector3 randomTorque = new Vector3(Random.Range(-30, 30), 3, Random.Range(-30, 30)) * _DeathExplotionPower;
        _Director.extrapolationMode = DirectorWrapMode.None;
        _Director.playableAsset = _SETimelineAsset;
        _Director.time = 0;
        _Director.Play();
        _Rigidbody.AddForceAtPosition(randomForce, damagePosition);
        _DeathParticle?.Play();
        _AnimationStartSubject?.OnNext(Unit.Default);

        await UniTask.Delay(TimeSpan.FromSeconds(_DeathAnimationTime));

        _Rigidbody.AddForce(randomForce + randomForce, ForceMode.VelocityChange);
        _Rigidbody.AddTorque(randomTorque);
        _Collider.isTrigger = true;
        _DeathParticleAfterAnimation?.Play();
        _AnimationEndSubject?.OnNext(Unit.Default);
    }
    Collider _Collider;

    public void Initialize(ScriptableTankStatus status)
    {
        if (_DeathParticleReference == null || _DeathParticleAfterAnimationReference == null)
        {
            Debug.Log("死にパーティクルないよ");
            return;
        }
        _Collider = GetComponent<Collider>();
        ParticleLoad();
    }

    async void ParticleLoad()
    {
        var deathParticle = _DeathParticleReference.InstantiateAsync(transform);
        var deathParticleAfterAnimation = _DeathParticleAfterAnimationReference.InstantiateAsync(transform);

        await deathParticle.Task;
        await deathParticleAfterAnimation.Task;

        if (deathParticle.Status == UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationStatus.Succeeded)
        {
            _DeathParticle = deathParticle.Result.GetComponent<ParticleSystem>();
        }
        if (deathParticleAfterAnimation.Status == UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationStatus.Succeeded)
        {
            _DeathParticleAfterAnimation = deathParticleAfterAnimation.Result.GetComponent<ParticleSystem>();
        }
    }

    void OnDestroy()
    {
        Addressables.ReleaseInstance(_DeathParticle.gameObject);
        Addressables.ReleaseInstance(_DeathParticleAfterAnimation.gameObject);
    }
}
