using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankRotate : MonoBehaviour, IStatusInitializable
{
    Transform _Transform;

    public void Initialize(ScriptableTankStatus status)
    {
        _Transform = transform;
    }

    public void Rotate(float angle)
    {
        _Transform.rotation = Quaternion.Euler(_Transform.rotation.x, angle, _Transform.rotation.z);
    }
}
