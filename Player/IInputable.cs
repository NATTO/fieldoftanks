using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public interface IInputable
{
    void InputUpdate();
    Vector2 MoveValue { get; }
    Vector2 RotateValue { get; }
    bool ShotButtonDown { get; }
    bool StopButtonDown { get; }
}
