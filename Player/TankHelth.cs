using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.AddressableAssets;

[RequireComponent(typeof(Rigidbody))]
public class TankHelth : MonoBehaviour, IStatusInitializable,IDamagable
{
    [SerializeField]
    ReactiveProperty<float> _Helth;
    [SerializeField]
    TankDeathAnimation _DeathAnimation;
    [SerializeField]
    SE_ID _DamageSE;

    public ReactiveProperty<float> HelthReactiveProperty { get => _Helth; }

    public void Initialize(ScriptableTankStatus status)
    {
        _Helth = new ReactiveProperty<float> { Value = status._TankLife };
    }


    public void TakeDamage(float damage, Vector3 damagePosition)
    {
        if (_Helth.Value == 0) return;
        if (_Helth.Value - damage <= 0)
            damage = _Helth.Value;

        _Helth.Value -= damage;
        SoundManager.Instance.SEPlay(_DamageSE);

        if (_Helth.Value == 0)
        {
            _DeathAnimation.AnimationStart(damagePosition);
        }
    }

}
