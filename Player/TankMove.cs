using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class TankMove : MonoBehaviour, IStatusInitializable
{
    const float _StopRay = 1;
    [SerializeField]
    float _Speed;
    [SerializeField]
    Rigidbody _Rigidbody;
    Transform _Transform;

    public void Initialize(ScriptableTankStatus status)
    {
        _Transform = transform;
        _Speed = status._TankSpeed;
    }

    public Vector3 MoveVector(Vector3 moveValue) => _Speed * Time.deltaTime * moveValue;

    public void Move(Vector2 moveValue, int moveLayerMask = 0)
    {
        Vector3 moveVector = new Vector3(moveValue.x, 0, moveValue.y).normalized;
        Ray ray = new Ray(_Transform.position, moveVector);
        if (!Physics.Raycast(ray, out var hit, _StopRay, moveLayerMask))
        {
            if (tag == TagNames.Enemy)
                _Rigidbody.position = _Transform.position + MoveVector(moveVector);
            else
                _Transform.position = _Transform.position + MoveVector(moveVector);
        }

    }
}
