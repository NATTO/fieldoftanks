﻿
using UnityEngine;
using UnityEngine.Playables;

public class PlayerSEBehaviour : PlayableBehaviour
{
    public SE_ID[] _PlayeSEs;
    public override void OnBehaviourPlay(Playable playable, FrameData info)
    {
        SE_ID playSE = _PlayeSEs[Random.Range(0, _PlayeSEs.Length)];
        SoundManager.Instance.SEPlay(playSE);
    }
    public override void OnBehaviourPause(Playable playable, FrameData info)
    {
        SE_ID playSE = _PlayeSEs[Random.Range(0, _PlayeSEs.Length)];
        SoundManager.Instance.SEPlay(playSE);
    }
}