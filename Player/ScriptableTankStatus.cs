using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Status/TankStatus")]
public class ScriptableTankStatus : ScriptableObject
{
    public float _BulletSpeed;
    public float _BulletDamage;
    public float _BulletReloadTime;
    public float _TankSpeed;
    public float _TankLife;
    public int _ReflectCount;
}
