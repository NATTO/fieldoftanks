using System.Collections;
using System.Collections.Generic;
using TNRD;
using UnityEngine;
using UniRx;
using System.Linq;
using System;

[RequireComponent(typeof(TankMove))]
[RequireComponent(typeof(TankShoot))]
[RequireComponent(typeof(TankRotate))]
[RequireComponent(typeof(TankHelth))]

public class PlayerManager : MonoBehaviour
{
    [SerializeField]
    SerializableInterface<IInputable> _Inputable;
    [SerializeField]
    TankMove _Move;
    [SerializeField]
    TankShoot _Shoot;
    [SerializeField]
    TankRotate _Rotate;
    [SerializeField]
    TankHelth _Helth;
    [SerializeField]
    PlayerAnimation _PlayerAnimation;
    [SerializeField]
    ScriptableTankStatus _TankStatus;
    Transform _Transform;
    Vector3 _BefourFramePosition;
    [SerializeField]
    public bool _IsMove = true;

    float _ReloadTime;
    public bool isReloadEnd => _TankStatus._BulletReloadTime <= _ReloadTime;

    public bool IsMove { get { return _IsMove; } set { _IsMove = value; } }

    private void Awake()
    {
        Array.ForEach(GetComponents<IStatusInitializable>(), x => x.Initialize(_TankStatus));
        _PlayerAnimation.Initialize();
        _Transform = transform;
    }

    private void Update()
    {
        if (_IsMove == false) return;
        if (_Helth.HelthReactiveProperty.Value == 0)
        {
            _PlayerAnimation.SetAnimationData(new AnimationData() { isDeath = true });
            return;
        }
        _Inputable.Value.InputUpdate();
        Vector2 normalizedMoveValue = _Inputable.Value.MoveValue.normalized;
        _Move.Move(normalizedMoveValue, LayerMasks.EnemyLayerIgnore & LayerMasks.CanThroughIgnore);
        Vector2 playerPos = new Vector2(_Transform.position.x, _Transform.position.z);
        float angle = GetControllerAngle(playerPos, _Inputable.Value.RotateValue);
        _Rotate.Rotate(angle);

        if (_Inputable.Value.ShotButtonDown && isReloadEnd)
        {
            _Shoot.Shoot(_Transform.forward, TagNames.Player);
            _ReloadTime = 0;
            SoundManager.Instance.SEPlay(SE_ID.Bullet_Shoot_Player);
        }
        _ReloadTime += Time.deltaTime;

        AnimationData animationData = new AnimationData()
        {
            isDeath = _Helth.HelthReactiveProperty.Value == 0,
            isShoot = _Inputable.Value.ShotButtonDown,
            angle = angle,
            moveX = _Inputable.Value.MoveValue.x,
            moveY = _Inputable.Value.MoveValue.y,
        };

        _PlayerAnimation.SetAnimationData(animationData);
    }

    float GetControllerAngle(Vector2 pos, Vector2 toPos)
    {
        Vector2 vector = toPos - pos;
        float radian = Mathf.Atan2(vector.x, vector.y);
        return radian * Mathf.Rad2Deg;
    }
}
