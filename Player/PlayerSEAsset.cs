using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

[System.Serializable]
public class PlayerSEAsset : PlayableAsset
{
    [SerializeField]
    SE_ID[] _SE;

    public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
    {
        PlayerSEBehaviour seBehaviour = new PlayerSEBehaviour();
        seBehaviour._PlayeSEs = _SE;
        ScriptPlayable<PlayerSEBehaviour> playable = ScriptPlayable<PlayerSEBehaviour>.Create(graph, seBehaviour);
        return playable;
    }
    
    
}
