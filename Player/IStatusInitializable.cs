using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// 戦車のステータス初期化
/// </summary>
public interface IStatusInitializable
{
    void Initialize(ScriptableTankStatus status);
}
