using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankShoot : MonoBehaviour, IStatusInitializable
{
    const float BulletHeight = 0.5f;

    [SerializeField]
    protected BulletMove _BulletObject;
    [SerializeField]
    protected Transform _BullelPosition;
    [SerializeField] 
    protected Rigidbody _Rigidbody;

    protected ScriptableTankStatus _Status;

    public virtual void Initialize(ScriptableTankStatus status) { _Status = status; }

    protected virtual BulletMove BulletInstance()
    {
        var bulletObject = Instantiate(_BulletObject);
        Vector3 position = _BullelPosition.position;

        Ray groundRay = new Ray(position, Vector3.down);
        if (Physics.Raycast(groundRay, out RaycastHit hit, Mathf.Infinity, LayerMasks.OnlyGround))
        {
            if(BulletHeight != hit.distance)
            {
                position.y = hit.collider.transform.position.y + BulletHeight;
            }
        }

        position.y = BulletHeight;
        bulletObject.transform.position = position;
        bulletObject.Initialize(_Status);
        return bulletObject;
    }

    public virtual void Shoot(Vector3 direction,string ignoreTag = "")
    {
        var bulletObject = BulletInstance();
        bulletObject.SetIgnoreTag(ignoreTag);
        bulletObject.MoveStart(direction);
    }
}
