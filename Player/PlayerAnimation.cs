using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using UnityEngine.Timeline;
using UnityEngine.Playables;
using UnityEngine.UI;

public class PlayerAnimation : MonoBehaviour
{
    [SerializeField]
    AnimationClip _Clip;
    [SerializeField]
    Animator _PlayerAnimator;
    [SerializeField]
    PlayableDirector _Director;
    [SerializeField]
    TimelineAsset _WalkSE;
    [SerializeField]
    TimelineAsset _DeathSE;
    bool _IsDeath;
    Transform _Transform;
    AnimationClip _BefourClip;

    public void Initialize()
    {
        _Transform = transform;
    }

    public void SetAnimationData(AnimationData data)
    {
        if (_IsDeath == true) return;
        _BefourClip = _Clip;
        if (_Clip == AnimationClip.Shoot)
        {
            var stateinfo = _PlayerAnimator.GetCurrentAnimatorStateInfo(0);
            if (stateinfo.normalizedTime <= 1f) return;
        }
        _IsDeath = data.isDeath;

        Vector3 moveData = _Transform.position + new Vector3(data.moveX, 0, data.moveY);
        Vector3 basePosition = _Transform.position;
        float[] moveDistances = new float[4]
        {
            (moveData - (basePosition + _Transform.forward)).sqrMagnitude,
            (moveData - (basePosition + -_Transform.forward)).sqrMagnitude,
            (moveData - (basePosition + _Transform.right)).sqrMagnitude,
            (moveData - (basePosition + -_Transform.right)).sqrMagnitude,
        };
        int maxIndex = Array.IndexOf(moveDistances, moveDistances.Max(distance => distance));

        switch (maxIndex)
        {
            case 0:
                _Clip = AnimationClip.WalkFront;
                break;
            case 1:
                _Clip = AnimationClip.WalkBack;
                break;
            case 2:
                _Clip = AnimationClip.WalkRight;
                break;
            case 3:
                _Clip = AnimationClip.WalkLeft;
                break;
            default:
                break;
        }

        if (data.moveX == 0 && data.moveY == 0)
        {
            _Clip = AnimationClip.Idle;
        }
        if (data.isShoot == true)
        {
            _Clip = AnimationClip.Shoot;
        }
        if(_Clip == AnimationClip.WalkBack || _Clip == AnimationClip.WalkLeft || _Clip == AnimationClip.WalkFront || _Clip == AnimationClip.WalkRight)
        {
            if(_BefourClip != _Clip)
            {
                _Director.playableAsset = _WalkSE;
                _Director.time = 0;
                _Director.Play();
            }
        }
        if(_Clip == AnimationClip.Idle)
        {
            _Director.Stop();
            _Director.time = 0;
        }

        if (_IsDeath == true)
        {
            _Clip = AnimationClip.Die;
            _Director.playableAsset = _DeathSE;
            _Director.time = 0;
            _Director.Play();
        }
        _PlayerAnimator.Play(_Clip.ToString());
    }
}

public struct AnimationData
{
    public bool isDeath;
    public bool isShoot;
    public float moveX;
    public float moveY;
    public float angle;
}
public enum AnimationClip
{
    Idle,
    Shoot,
    WalkFront,
    WalkBack,
    WalkRight,
    WalkLeft,
    Die
}