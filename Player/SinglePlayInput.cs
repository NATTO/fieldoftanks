using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.InputSystem;

public class SinglePlayInput : MonoBehaviour, IInputable
{
    enum InputType
    {
        KeyBoard,
        GamePad,
    }
    [SerializeField] InputType _InputType = InputType.KeyBoard;

    Vector2 _MoveValue;
    public Vector2 MoveValue => _MoveValue;

    Vector2 _RotateValue;
    public Vector2 RotateValue => _RotateValue;

    bool _ShotButtonDown;
    public bool ShotButtonDown => _ShotButtonDown;

    bool _StopButtonDown;
    public bool StopButtonDown => _StopButtonDown;

    Plane _CameraPlane = new Plane();
    [SerializeField]
    Camera _TargetCamera;
    bool IsMouseInput()
    {
        return Mouse.current.middleButton.IsPressed() ||
            Mouse.current.leftButton.IsPressed() || 
            Mouse.current.rightButton.IsPressed() || 
            Pointer.current.IsPressed();
    }
    public void InputUpdate()
    {
        if (Keyboard.current.anyKey.IsPressed() || IsMouseInput())
        {
            _InputType = InputType.KeyBoard;
        }
        else if (Gamepad.all.Count != 0)
        {
            if (Gamepad.current.IsPressed())
                _InputType = InputType.GamePad;
        }

        InputInitialize();
        switch (_InputType)
        {
            case InputType.KeyBoard:
                KeyBoardInput();
                break;
            case InputType.GamePad:
                GamePadInput();
                break;
        }
    }

    void InputInitialize()
    {
        _ShotButtonDown = false;
        _StopButtonDown = false;
        _MoveValue = Vector2.zero;
        _RotateValue = Vector2.zero;
    }

    void GamePadInput()
    {
        _MoveValue = Gamepad.current.leftStick.value;
        _RotateValue = Gamepad.current.rightStick.value;
        if (Gamepad.current.buttonEast.wasPressedThisFrame ||
            Gamepad.current.buttonNorth.wasPressedThisFrame ||
            Gamepad.current.buttonSouth.wasPressedThisFrame ||
            Gamepad.current.buttonWest.wasPressedThisFrame)
        {
            _ShotButtonDown = true;
        }
        if (Gamepad.current.startButton.wasPressedThisFrame)
        {
            _StopButtonDown = true;
        }
    }


    void KeyBoardInput()
    {
        _MoveValue = Vector2.zero;
        if ((Keyboard.current.leftArrowKey.value + Keyboard.current.aKey.value) >= 1)
            _MoveValue.x -= 1;
        if ((Keyboard.current.rightArrowKey.value + Keyboard.current.dKey.value) >= 1)
            _MoveValue.x += 1;
        if ((Keyboard.current.downArrowKey.value + Keyboard.current.sKey.value) >= 1)
            _MoveValue.y -= 1;
        if ((Keyboard.current.upArrowKey.value + Keyboard.current.wKey.value) >= 1)
            _MoveValue.y += 1;

        _TargetCamera = Camera.main;

        var cameraRay = Camera.main.ScreenPointToRay(Mouse.current.position.value);
        _CameraPlane.SetNormalAndPosition(Vector3.up, transform.localPosition);
        Vector2 point = Vector2.zero;
        if (_CameraPlane.Raycast(cameraRay, out float distance))
        {            
            var p = cameraRay.GetPoint(distance);
            point = new Vector2(p.x, p.z);
        }
        _RotateValue = point;

        if (Mouse.current.leftButton.wasPressedThisFrame || Mouse.current.rightButton.wasPressedThisFrame)
        {
            _ShotButtonDown = true;
        }

        if (Keyboard.current.escapeKey.wasPressedThisFrame)
        {
            _StopButtonDown = true;
        }
    }
}
