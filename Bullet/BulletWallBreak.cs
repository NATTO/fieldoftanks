using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletWallBreak : MonoBehaviour
{

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag != TagNames.CanBreakWall) return;
    }
}
