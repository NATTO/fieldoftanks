using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using Unity.VisualScripting;
using UnityEngine;

/// <summary>
/// 銃弾を動かす奴
/// </summary>
[RequireComponent(typeof(Rigidbody))]
public class BulletMove : MonoBehaviour
{
    const int _CollisionWaitFrameCount = 5;

    /// <summary>
    /// ダメージ
    /// </summary>
    float _BulletDamage;

    /// <summary>
    /// 速度
    /// </summary>
    float _BulletSpeed;

    /// <summary>
    /// 反射限界回数
    /// </summary>
    int _ReflectCount;

    /// <summary>
    /// 今動いてるベクトル
    /// </summary>
    [SerializeField]
    Vector3 _MoveVector;

    Rigidbody _Rigidbody;

    /// <summary>
    /// ダメージ判定しないタグ
    /// </summary>
    [SerializeField]
    string _IgnoreTag = string.Empty;

    /// <summary>
    /// この弾が衝突判定するか
    /// </summary>
    bool _IsBulletCollision = false;

    /// <summary>
    /// 無視タグ設定
    /// </summary>
    /// <param name="ignoreTag"></param>
    public void SetIgnoreTag(string ignoreTag)
    {
        _IgnoreTag = ignoreTag;
    }

    public void Initialize(ScriptableTankStatus status)
    {
        _IsBulletCollision = false;
        _BulletDamage = status._BulletDamage;
        _BulletSpeed = status._BulletSpeed;
        _ReflectCount = status._ReflectCount;
        _Rigidbody ??= GetComponent<Rigidbody>();
    }

    /// <summary>
    /// 動かす
    /// </summary>
    /// <param name="startVector">行く方向</param>
    /// <returns></returns>
    public void MoveStart(Vector3 startVector)
    {

        StartCoroutine(MoveCoroutine());
        BulletCollisionWaitTask(destroyCancellationToken);

        IEnumerator MoveCoroutine()
        {
            _MoveVector = startVector;
            transform.rotation = Quaternion.LookRotation(_MoveVector, Vector3.up);

            var waitFrame = new WaitForFixedUpdate();
            while (true)
            {
                Vector3 moveVec = _MoveVector * Time.deltaTime * _BulletSpeed;
                _Rigidbody.velocity = moveVec;

                yield return waitFrame;
            }
        }

        async UniTask BulletCollisionWaitTask(CancellationToken token)
        {
            await UniTask.DelayFrame(_CollisionWaitFrameCount, PlayerLoopTiming.FixedUpdate, token);
            _IsBulletCollision = true;
        }
    }

    Vector3 ReflectVector(Vector3 inVector, Vector3 normal)
    {
        return Vector3.Reflect(inVector, normal);
    }

    private void OnCollisionEnter(Collision collision)
    {
        string collisionTag = collision.transform.tag;
        //壊せる壁か跳ね返らない壁かに当たったら
        if (collisionTag == TagNames.CanBreakWall || collisionTag == TagNames.NonReflectWall)
        {
            if (collisionTag == TagNames.Bullet && _IsBulletCollision == false)
                return;
            //壊す
            Destroy(gameObject);
            return;
        }

        //無視するかの判定
        bool isGetHelth = collision.collider.TryGetComponent(out IDamagable helth);
        bool isIgnoreTag = false;
        if (_IgnoreTag == string.Empty)
            isIgnoreTag = true;
        else
            isIgnoreTag = collision.gameObject.CompareTag(_IgnoreTag) == false;

        //ダメージ判定
        if (isGetHelth == true && isIgnoreTag)
        {
            helth.TakeDamage(_BulletDamage, transform.position);
            Destroy(gameObject);
            return;
        }

        //反射
        _MoveVector = ReflectVector(_MoveVector, collision.contacts[0].normal);
        transform.rotation = Quaternion.LookRotation(_MoveVector, Vector3.up);

        //反射限界で壊す、そうでもないならSE鳴らす
        if (_ReflectCount <= 0)
            Destroy(gameObject);
        else SoundManager.Instance.SEPlay(SE_ID.Bullet_Reflect);

        _ReflectCount--;
    }
}
