using Cysharp.Threading.Tasks;
using System;
using System.Threading;
using UniRx;
using UnityEngine;

public class PlayerDeathCamera : MonoBehaviour
{
    [SerializeField]
    Camera _PlayCamera;
    [SerializeField]
    Camera _DeathCamera;
    [SerializeField]
    TankDeathAnimation _PlayerDeathAnimation;
    [SerializeField]
    Vector3 _DeathCameraPositionOffset;
    [SerializeField, Range(0, 180)]
    float _DeathCameraInitializeFieldOfView;
    [SerializeField, Range(0, 180)]
    float _DeathCameraAnimationFieldOfView;

    private void Awake()
    {
        _PlayerDeathAnimation.AnimationStartObservable.First()
            .Subscribe(_ => PlayerDeath(destroyCancellationToken));

    }

    /// <summary>
    /// プレイヤーが死んだときのカメラアニメーション
    /// </summary>
    /// <param name="cancellation"></param>
    /// <returns></returns>
    async UniTask PlayerDeath(CancellationToken cancellation)
    {
        _PlayCamera.gameObject.SetActive(false);
        _DeathCamera.gameObject.SetActive(true);
        _DeathCamera.transform.position = _PlayerDeathAnimation.transform.position + _DeathCameraPositionOffset;
        _DeathCamera.transform.LookAt(_PlayerDeathAnimation.transform);
        _DeathCamera.fieldOfView = _DeathCameraInitializeFieldOfView;

        float time = 0;
        while (time < _PlayerDeathAnimation.DeathAnimationTime)
        {
            _DeathCamera.fieldOfView = Mathf.Lerp(_DeathCameraInitializeFieldOfView, _DeathCameraAnimationFieldOfView, time / _PlayerDeathAnimation.DeathAnimationTime);
            _DeathCamera.transform.LookAt(_PlayerDeathAnimation.transform);
            time += Time.deltaTime;
            await UniTask.NextFrame(cancellation);
        }
        _DeathCamera.fieldOfView = _DeathCameraAnimationFieldOfView;
    }
}
