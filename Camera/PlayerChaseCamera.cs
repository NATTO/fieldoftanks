using UnityEngine;

public class PlayerChaseCamera : MonoBehaviour
{
    [SerializeField]
    PlayerManager _PlayerTransform;
    [SerializeField]
    Camera _MainCamera;
    [SerializeField]
    float _ChaseSpeed;
    [SerializeField]
    Vector3 _PlayerCameraFarAway;

    Transform _CameraTransform;

    private void Awake()
    {
        _CameraTransform = _MainCamera.transform;        
    }

    private void Update()
    {
        //カメラがたどり着いたら返す
        if (_CameraTransform.position == _PlayerTransform.transform.position + _PlayerCameraFarAway) return;

        float distance = ((_PlayerTransform.transform.position + _PlayerCameraFarAway) - _CameraTransform.position).sqrMagnitude;
        Vector3 targetPosition = _PlayerTransform.transform.position + _PlayerCameraFarAway;

        //追従
        _CameraTransform.position = Vector3.MoveTowards(_CameraTransform.position, targetPosition, _ChaseSpeed * Time.deltaTime * distance);
    }
}
