using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundConfigChanger : MonoBehaviour
{
    [SerializeField]
    private Slider _BGMSlider;
    [SerializeField]
    private Slider _SESlider;
    private void Awake()
    {
        _BGMSlider.value = SaveDataManager.LoadFloat(SaveDataManager.BGMDataPath);
        _SESlider.value = SaveDataManager.LoadFloat(SaveDataManager.SEDataPath);
    }
    public void BGMChange(Slider value)
    {
        SoundConfig.SetBGMPropertyValue(value.value);
    }
    public void SEChange(Slider value)
    {
        SoundConfig.SetSEPropertyValue(value.value);
    }

    public void Save()
    {
        SoundConfig.SaveProperties();
    }
}
