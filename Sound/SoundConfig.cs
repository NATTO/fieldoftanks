using UniRx;
public static class SoundConfig
{
    static ReactiveProperty<float> _BGMProperty = new ReactiveProperty<float>();
    static ReactiveProperty<float> _SEProperty = new ReactiveProperty<float>();

    public static ReadOnlyReactiveProperty<float> BGMProperty => _BGMProperty.ToReadOnlyReactiveProperty();

    public static ReadOnlyReactiveProperty<float> SEProperty => _SEProperty.ToReadOnlyReactiveProperty();


    public static void SetBGMPropertyValue(float value) => _BGMProperty.Value = value;

    public static void SetSEPropertyValue(float value) => _SEProperty.Value = value;

    public static void SaveProperties()
    {
        SaveDataManager.SaveFloat(SaveDataManager.BGMDataPath, _BGMProperty.Value);
        SaveDataManager.SaveFloat(SaveDataManager.SEDataPath, _SEProperty.Value);
    }
}
