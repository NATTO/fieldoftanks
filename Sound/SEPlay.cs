using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SEPlay : MonoBehaviour
{
    public void PlayOneShot(AudioClip clip) => SoundManager.Instance.SEPlay(clip);
}
