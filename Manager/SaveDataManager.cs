﻿using Cysharp.Text;
using System;
using System.IO;
using UnityEngine;

/// <summary>
/// セーブデータ管理するやつ
/// </summary>
static class SaveDataManager
{
    public const string LevelDataPath = "Level.txt";
#if UNITY_EDITOR
    static readonly string _SaveDataPath = Application.dataPath + "/" + "SaveData";
#endif
#if (UNITY_STANDALONE_WIN || UNITY_WEBGL) && !UNITY_EDITOR
    static readonly string _SaveDataPath = Application.dataPath + "/" + "SaveData";
#endif
    public static readonly string BGMDataPath = "BGM.txt";
    public static readonly string SEDataPath = "SE.txt";
    const string _FirstStartKey = "FirstKey";
    const int _StartLevel = 0;
    const float _StartBGMVolume = 1.0f;
    const float _StartSEVolume = 1.0f;

    public static void SaveInitialize()
    {
        if (string.IsNullOrEmpty(LoadString(_FirstStartKey)))
        {
            SaveInt(LevelDataPath, _StartLevel);
            SaveFloat(BGMDataPath, _StartBGMVolume);
            SaveFloat(SEDataPath, _StartSEVolume);
            SaveString(_FirstStartKey, _FirstStartKey);
        }
    }

    [Serializable]
    struct SaveData<T> { public T value; }
    public static void SaveFloat(string path, float value)
    {
        SaveData<float> data = new SaveData<float>() { value = value };
        Save(path, data);
    }
    public static void SaveInt(string path, int value)
    {
        SaveData<int> data = new SaveData<int>() { value = value };
        Save(path, data);
    }
    public static void SaveString(string path, string value)
    {
        SaveData<string> data = new SaveData<string>() { value = value };
        Save(path, data);
    }
    public static void SaveBool(string path, bool value)
    {
        SaveData<bool> data = new SaveData<bool>() { value = value };
        Save(path, data);
    }

    /// <summary>
    /// セーブ
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="path"></param>
    /// <param name="value"></param>
    public static void Save<T>(string path, T value)
    {
        string jsonData = JsonUtility.ToJson(value);
        path = _SaveDataPath + "/" + path;
        FileInfo fileInfo = new FileInfo(path);

        if (!Directory.Exists(fileInfo.DirectoryName))
        {
            Directory.CreateDirectory(fileInfo.DirectoryName);
        }

        try
        {
            using (var writer = new StreamWriter(path))
            {
                writer.WriteLine(jsonData);
                writer.Close();
            }
        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }
    }
    public static float LoadFloat(string path)
    {
        return Load<SaveData<float>>(path).value;
    }
    public static int LoadInt(string path)
    {
        return Load<SaveData<int>>(path).value;
    }
    public static string LoadString(string path)
    {
        return Load<SaveData<string>>(path).value;
    }
    public static bool LoadBool(string path)
    {
        return Load<SaveData<bool>>(path).value;
    }

    /// <summary>
    /// ロード
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="path"></param>
    /// <returns></returns>
    public static T Load<T>(string path)
    {
        if (!File.Exists(ZString.Concat(_SaveDataPath, "/", path)))
        {
            Debug.Log("ファイル無いね");
            return default(T);
        }
        T result = default(T);
        try
        {
            using (var reader = new StreamReader(ZString.Concat(_SaveDataPath, "/", path)))
            {
                string jsonData = reader.ReadToEnd();
                result = JsonUtility.FromJson<T>(jsonData);
                reader.Close();
            }
        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }
        return result;
    }
}