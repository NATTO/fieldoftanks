using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System.Linq;
using UnityEngine.InputSystem;

/// <summary>
/// 敵全体を管理
/// </summary>
[DefaultExecutionOrder(-1)]
public class EnemyManager : MonoBehaviour
{
    [SerializeField]
    PlayerManager _Player;

    [SerializeField]
    List<EnemyController> _Enemies = new List<EnemyController>();

    List<TankHelth> _EnemyHelths = new List<TankHelth>();
    int _DeathEnemyCount = 0;

    //敵の数
    ReactiveProperty<int> _EnemyCount = new ReactiveProperty<int>();
    public ReactiveProperty<int> EnemyCount => _EnemyCount;

    //敵が全員死んだときのイベント
    Subject<Unit> _AllEnemyDeathSubject = new Subject<Unit>();
    public Subject<Unit> AllEnemyDeathSubject => _AllEnemyDeathSubject;

    private void Awake()
    {
        if (_Player != null)
            Initialize(_Player);
    }

#if UNITY_EDITOR
    //敵ぶっころすデバッグコマンド
    private void Update()
    {
        if(Keyboard.current.f10Key.wasPressedThisFrame)
            _Enemies.ConvertAll(x => x.GetComponent<TankHelth>()).ForEach(x => x.TakeDamage(1000,Vector3.zero));
    }
#endif

    public void Initialize(PlayerManager playerManager)
    {
        //敵全員初期化
        foreach (var enemy in _Enemies)
            enemy.Initialize(playerManager);

        //敵の命取得
        _EnemyHelths = _Enemies.ConvertAll(enemy => enemy.GetComponent<TankHelth>());
        _EnemyCount.Value = _Enemies.Count;

        foreach (var enemy in _EnemyHelths)
        {
            enemy.HelthReactiveProperty.Subscribe(EnemyLifeChange).AddTo(gameObject);
        }
    }

    //敵のHP減ったりなんだりした時のイベント
    void EnemyLifeChange(float life)
    {
        if (life != 0) return;
        //死んだらカウント
        _DeathEnemyCount++;
        _EnemyCount.Value--;

        if (_DeathEnemyCount >= _Enemies.Count)
            GameClear();
    }

    void GameClear()
    {
        Debug.Log("かち");
        _AllEnemyDeathSubject.OnNext(Unit.Default);
    }

#if UNITY_EDITOR
    /// <summary>
    /// 同一シーン内にある敵のオブジェクト全取得
    /// </summary>
    [ContextMenu("ユニットの読み込み")]
    void LoadSceneUnits()
    {
        _Player = FindFirstObjectByType<PlayerManager>();

        _Enemies = FindObjectsByType<EnemyController>(FindObjectsSortMode.None).ToList();
    }
#endif
}
