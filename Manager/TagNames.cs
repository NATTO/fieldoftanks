﻿
public static class TagNames
{
    public const string NonReflectWall = "NONREFLECTWALL";
    public const string CanBreakWall = "BREAKWALL";
    public const string Player = "PLAYER";
    public const string ManagerObject = "MANAGER";
    public const string Enemy = "Enemy";
    public const string Bullet = "BULLET";
}