using UnityEditor;
using UnityEngine;

/// <summary>
/// ステージセレクトいくやつ
/// </summary>
public class MenuButton : MonoBehaviour
{
    [SerializeField]
    RectTransform _StageSelectParent;
    public void GameStart() => _StageSelectParent.gameObject.SetActive(true);

    public void QuitGame()
    {
#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#endif
        Application.Quit();
    }
}
