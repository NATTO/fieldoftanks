using UnityEngine;

/// <summary>
/// 最初に初期化する奴
/// </summary>
public class AwakeManagerInitializer : MonoBehaviour
{
    [SerializeField]
    SoundManager _SoundManager;
    [SerializeField]
    SceneLoadManager _SceneLoadManager;

    async void Awake()
    {
        SaveDataManager.SaveInitialize();
        await _SoundManager.Initialize();
        _SceneLoadManager.Initialize();
    }
}
