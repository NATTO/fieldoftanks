﻿using Cysharp.Threading.Tasks;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UniRx;
/// <summary>
/// BGMとSEを管理するクラス
/// </summary>
[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(AudioSource))]
public class SoundManager : SingletonMonoBehaviour<SoundManager>
{
    const float DEFAULT_FADEOUT_TIME = 1f;

    AudioSource _BGMAudioSource;
    AudioSource _SEAudioSource;

    Dictionary<BGM_ID, AudioClip> _BGMClipReferences = new Dictionary<BGM_ID, AudioClip>();
    Dictionary<SE_ID, AudioClip> _SEClipsReferences = new Dictionary<SE_ID, AudioClip>();

    [SerializeField]
    List<SoundClip<BGM_ID>> _BGMClips;
    [SerializeField]
    List<SoundClip<SE_ID>> _SEClips;
    List<AsyncOperationHandle<AudioClip>> _AudioLoadAsset = new List<AsyncOperationHandle<AudioClip>>();

    /// <summary>
    /// 初期化
    /// </summary>
    /// <returns></returns>
    public async UniTask Initialize()
    {
        base.Awake();
        var audios = GetComponents<AudioSource>();
        _BGMAudioSource = audios[0];
        _SEAudioSource = audios[1];

        TimeoutController timeoutController = new TimeoutController();
        var timeout = timeoutController.Timeout(TimeSpan.FromSeconds(5));

        //AddressablesからBGMとSEロード
        foreach (var clipValue in _BGMClips)
        {
            var loadClipAsset = clipValue._Clip.LoadAssetAsync();
            AudioClip clip = await loadClipAsset.Task;
            _BGMClipReferences.Add(clipValue._ID, clip);
            _AudioLoadAsset.Add(loadClipAsset);
        }
        foreach (var clipValue in _SEClips)
        {
            var loadClipAsset = clipValue._Clip.LoadAssetAsync();
            AudioClip clip = await loadClipAsset.Task;
            _SEClipsReferences.Add(clipValue._ID, clip);
            _AudioLoadAsset.Add(loadClipAsset);
        }
        _BGMAudioSource.volume = SaveDataManager.LoadFloat(SaveDataManager.BGMDataPath);
        _SEAudioSource.volume = SaveDataManager.LoadFloat(SaveDataManager.SEDataPath);

        SoundConfig.BGMProperty.Skip(1).Subscribe(vol => _BGMAudioSource.volume = vol);
        SoundConfig.SEProperty.Skip(1).Subscribe(vol => _SEAudioSource.volume = vol);
    }

    private void OnDestroy()
    {
        foreach (var i in _AudioLoadAsset) Addressables.Release(i);
    }

    public void SEPlay(AudioClip clip)
    {
        _SEAudioSource.PlayOneShot(clip);
    }

    public void SEPlay(SE_ID id)
    {
        if (_SEClipsReferences.ContainsKey(id))
        {
            _SEAudioSource.PlayOneShot(_SEClipsReferences[id]);
        }
        else
        {
            Debug.LogError($"{id}が存在しません");
        }
    }

    public void BGMPlay(BGM_ID id)
    {
        if (_BGMClipReferences.ContainsKey(id))
        {
            _BGMAudioSource.clip = _BGMClipReferences[id];
            _BGMAudioSource.Play();
        }
        else
        {
            Debug.LogError($"{id}が存在しません");
        }
    }

    public void BGMFadeOut(float time = DEFAULT_FADEOUT_TIME)
    {
        StartCoroutine(BGMFadeOutCoroutine(time));
    }

    public void BGMReStart()
    {
        _BGMAudioSource.Play();
    }

    /// <summary>
    /// BGMフェードアウトするやつ
    /// </summary>
    /// <param name="time"></param>
    /// <returns></returns>
    IEnumerator BGMFadeOutCoroutine(float time)
    {
        float startValue = _BGMAudioSource.volume;
        float progress = startValue;
        float t = 0;
        while (progress > 0)
        {
            _BGMAudioSource.volume = progress;
            progress = Mathf.Lerp(startValue, 0, t);
            t += Time.deltaTime / time;
            yield return null;
        }
        _BGMAudioSource.Stop();
        _BGMAudioSource.volume = startValue;
    }
}

[System.Serializable]
struct SoundClip<T> where T : struct, Enum
{
    public T _ID;
    public AssetReferenceT<AudioClip> _Clip;
}

public enum BGM_ID
{
    Menu,
    Stage_1,
    Stage_2,
}

public enum SE_ID
{
    Bullet_Shoot_Player,
    Bullet_Shoot_Enemy,
    Bullet_Reflect,
    Move_Player_1,
    Move_Player_2,
    Move_Player_3,
    Move_Player_4,
    Move_Player_5,
    Button_Click,
    Damage_Player,
    Damage_Enemy,
    Death_Enemy,
    Death_Enemy_2,
    Death_Player,
    Death_Player_2,
    Clear,
}