using Cysharp.Threading.Tasks;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// シーンロード系管理
/// </summary>
public class SceneLoadManager : SingletonMonoBehaviour<SceneLoadManager>
{
    /// <summary>
    /// 最初に読み込むシーン
    /// </summary>
    [SerializeField]
    string _StartSceneName;

    [SerializeField]
    Canvas _LoadCanvas;
    [SerializeField]
    Image _LoadImage;
    [SerializeField]
    float _AnimationTime;

    public void Initialize()
    {
        LoadSceneSync(_StartSceneName);
    }

    public void LoadSceneSync(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    /// <summary>
    /// 非同期ロード（アニメーション付き）
    /// </summary>
    /// <param name="sceneId">ビルド番号</param>
    /// <param name="loadMode"></param>
    /// <param name="loadEndAction">ロード終わる時のアクション</param>
    /// <returns></returns>
    public async UniTask LoadSceneAsync(int sceneId, LoadSceneMode loadMode = LoadSceneMode.Single, Action loadEndAction = null)
    {
        _LoadCanvas.gameObject.SetActive(true);
        await FadeAnimation(FadeMode.InFade);
        await SceneManager.LoadSceneAsync(sceneId, loadMode);
        loadEndAction?.Invoke();
        await FadeAnimation(FadeMode.OutFade);
        _LoadCanvas.gameObject.SetActive(false);
    }

    /// <summary>
    /// 非同期ロード（アニメーション付き）
    /// </summary>
    /// <param name="sceneName">シーンの名前</param>
    /// <param name="loadMode"></param>
    /// <param name="loadEndAction">ロード終わる時のアクション</param>
    /// <returns></returns>
    public async UniTask LoadSceneAsync(string sceneName, LoadSceneMode loadMode = LoadSceneMode.Single,Action loadEndAction = null)
    {
        _LoadCanvas.gameObject.SetActive(true);
        await FadeAnimation(FadeMode.InFade);
        await SceneManager.LoadSceneAsync(sceneName, loadMode);
        loadEndAction?.Invoke();
        await FadeAnimation(FadeMode.OutFade);
        _LoadCanvas.gameObject.SetActive(false);
    }

    /// <summary>
    /// アニメーション
    /// </summary>
    /// <param name="fadeMode"></param>
    /// <returns></returns>
    async UniTask FadeAnimation(FadeMode fadeMode)
    {
        float progressTime = 0;
        float fadeProgress = fadeMode switch
        {
            FadeMode.InFade => 0,
            FadeMode.OutFade => 1,
        };
        Color fadeColor = _LoadImage.color;
        fadeColor.a = fadeProgress;
        _LoadImage.color = fadeColor;

        while (progressTime < _AnimationTime)
        {
            _LoadImage.color = fadeColor;
            await UniTask.Yield();
            progressTime += Time.deltaTime;
            fadeColor.a = fadeMode switch
            {
                FadeMode.InFade => progressTime / _AnimationTime,
                FadeMode.OutFade => 1 - progressTime / _AnimationTime,
            };
        }

        fadeColor.a = fadeMode switch
        {
            FadeMode.InFade => 1,
            FadeMode.OutFade => 0,
        };
        _LoadImage.color = fadeColor;
    }
    enum FadeMode
    {
        InFade,
        OutFade
    }
}
