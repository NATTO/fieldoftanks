using UnityEngine;
using UnityEngine.SceneManagement;
using UniRx;
using System;
using Cysharp.Threading.Tasks;

/// <summary>
/// ステージ進行管理
/// </summary>
public class LevelProgressManager : SingletonMonoBehaviour<LevelProgressManager>
{
    public const int _StageBuildIndex = 3;
    [SerializeField]
    int _CurrentLevel;
    [SerializeField]
    EnemyManager _CurrentSceneEnemyManager;
    [SerializeField]
    PlayerManager _CurrentScenePlayerManager;
    [SerializeField]
    TankDeathAnimation _CurrentDeathAnimation;

    [SerializeField]
    LevelEndUI _LevelEndUI;

    [SerializeField]
    PlayerDeathUI _PlayerDeathUI;
    [SerializeField]
    InGameProgress _InGameProgress;
    [SerializeField]
    float _StartTime;
    [SerializeField]
    LevelStartAnimation _StartAnimationCanvas;
    [SerializeField]
    bool _IsDebug;
    string _StageName;
    public string StageName => _StageName;

    enum InGameProgress
    {
        In,
        End
    }

    private void Awake()
    {
        if (_IsDebug)
        {
            _CurrentLevel = SceneManager.GetActiveScene().buildIndex;
            DontDestroyOnLoad(gameObject);
            SceneInitialize();
        }
    }

    //次のシーン存在してるか
    public bool IsExistNextStage()
    {
        int maxCount = SceneManager.sceneCountInBuildSettings;
        Debug.Log(maxCount > _CurrentLevel);
        return maxCount > _CurrentLevel;
    }

    /// <summary>
    /// ステージ開始
    /// </summary>
    /// <param name="stage"></param>
    public async void ProgressStart(int stage)
    {
        //if (GetSaveDataLevel() == -1)
        //{
        //    await SceneLoadManager.Instance.LoadSceneAsync(SceneNames._TutorialScene);
        //    _CurrentLevel = SceneManager.GetSceneByName(SceneNames._TutorialScene).buildIndex;
        //}
        //else
        //{
        //    _CurrentLevel = GetSaveDataLevel() + _StageBuildIndex;
        //    await SceneLoadManager.Instance.LoadSceneAsync(_CurrentLevel);
        //}

        await SceneLoadManager.Instance.LoadSceneAsync(stage);
        _CurrentLevel = stage;

        SceneInitialize();
    }

    /// <summary>
    /// そのステージにある情報取得
    /// </summary>
    void SceneInitialize()
    {
        _CurrentSceneEnemyManager = FindAnyObjectByType<EnemyManager>();
        _CurrentScenePlayerManager = FindAnyObjectByType<PlayerManager>();
        _CurrentDeathAnimation = _CurrentScenePlayerManager.GetComponent<TankDeathAnimation>();
        _PlayerDeathUI.Initialize();

        _CurrentSceneEnemyManager.AllEnemyDeathSubject.Where(_ => _InGameProgress == InGameProgress.In).First()
            .Subscribe(_ => StageClear()).AddTo(_CurrentSceneEnemyManager);

        _CurrentDeathAnimation.AnimationEndObservable.Where(_ => _InGameProgress == InGameProgress.In).First()
            .Subscribe(_ => GameOver()).AddTo(_CurrentDeathAnimation);
        _LevelEndUI.Initialize();
        LevelStartAnimation();
    }

    /// <summary>
    /// アニメーション
    /// </summary>
    /// <returns></returns>
    async UniTask LevelStartAnimation()
    {
        _CurrentScenePlayerManager.IsMove = false;

        await _StartAnimationCanvas.AnimationStart(_StartTime);

        _CurrentScenePlayerManager.IsMove = true;
        _InGameProgress = InGameProgress.In;
    }

    int GetSaveDataLevel()
    {
        int result = -1;

        int savedata = SaveDataManager.LoadInt(SaveDataManager.LevelDataPath);

        if (savedata != 0)
        {
            result = savedata;
        }
        return result;
    }

    void GameOver()
    {
        SoundManager.Instance.BGMFadeOut();
        _InGameProgress = InGameProgress.End;
        _PlayerDeathUI.UIAnimation();
    }

    void StageClear()
    {
        SoundManager.Instance.BGMFadeOut();
        _InGameProgress = InGameProgress.End;
        _CurrentLevel++;
        //次のステージが存在しててセーブデータより進んでたらこのステージクリアしたことセーブする
        if (IsExistNextStage() == true && _CurrentLevel > SaveDataManager.LoadInt(SaveDataManager.LevelDataPath))
            SaveDataManager.SaveInt(SaveDataManager.LevelDataPath, _CurrentLevel - _StageBuildIndex + 1);
        _LevelEndUI.UIAnimation(this.destroyCancellationToken);
    }

    /// <summary>
    /// メニューシーンに戻る
    /// </summary>
    public void BackMenuScene()
    {
        BackAsync();
    }

    async UniTask BackAsync()
    {
        await SceneLoadManager.Instance.LoadSceneAsync(SceneNames._MenuScene, LoadSceneMode.Single,
            () =>
            {
                _LevelEndUI.Initialize();
                _PlayerDeathUI.Initialize();
            });
    }

    /// <summary>
    /// 次のステージに行く
    /// </summary>
    public void NextScene()
    {
        Scene nextScene;
        try
        {
            nextScene = SceneManager.GetSceneByBuildIndex(_CurrentLevel);
        }
        catch (Exception error)
        {
            Debug.Log(error.ToString());

            return;
        }
        _StageName = nextScene.name;

        SceneLoad();
    }
    public void ReTryScene()
    {
        RetrySceneTask();
    }

    public async UniTask RetrySceneTask()
    {
        await SceneLoadManager.Instance.LoadSceneAsync(_CurrentLevel - 1 + _StageBuildIndex);
        SceneInitialize();
    }

    async UniTask SceneLoad()
    {
        await SceneLoadManager.Instance.LoadSceneAsync(_CurrentLevel, default,
            () =>
            {
                _LevelEndUI.Initialize();
                _PlayerDeathUI.Initialize();
            });
        SceneInitialize();
    }
}
