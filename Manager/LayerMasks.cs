﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
static class LayerMasks
{
    public const int PlayerLayerIgnore = ~(1 << 3);
    public const int EnemyLayerIgnore = ~(1 << 6);
    public const int CanThroughIgnore = ~(1 << 9);
    public const int OnlyGround = 1 << 7;
}