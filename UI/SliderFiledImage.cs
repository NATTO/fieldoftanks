using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderFiledImage : MonoBehaviour
{
    [SerializeField]
    Image _FiledImage;


    public void Filed(Slider slider)
    {
        _FiledImage.fillAmount = slider.value;
    }
}
