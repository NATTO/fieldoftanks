﻿using Cysharp.Threading.Tasks;
using TMPro;
using UnityEngine;
using DG.Tweening;
using System;

/// <summary>
/// ステージ始まった時のUIアニメーション
/// </summary>
public class LevelStartAnimation : MonoBehaviour
{
    [field: SerializeField]
    float _AnimationTime { get; set; }

    [SerializeField]
    string _WaitText;
    [SerializeField]
    string _GoText;
    [SerializeField]
    Vector3 _WaitEndScale;
    [SerializeField]
    Vector3 _GoEndScale;
    [SerializeField]
    TextMeshProUGUI _Text;

    public async UniTask AnimationStart(float animationTime)
    {
        _Text.gameObject.SetActive(true);
        _Text.text = _WaitText;
        _AnimationTime = animationTime;

        var sequense = DOTween.Sequence();

        sequense.Append(_Text.transform.DOScale(_WaitEndScale, animationTime));
        sequense.AppendCallback(() => _Text.text = _GoText);
        sequense.Append(_Text.transform.DOScale(_GoEndScale, animationTime / 2));
        sequense.AppendCallback(() => _Text.gameObject.SetActive(false));
        sequense.Play();
        await UniTask.Delay(TimeSpan.FromSeconds(animationTime));
    }
}