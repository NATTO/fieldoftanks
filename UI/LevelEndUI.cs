using UnityEngine;
using UnityEngine.UI;
using Cysharp.Threading.Tasks;
using System.Threading;
using System;
using DG.Tweening;

/// <summary>
/// ステージ完了UI
/// </summary>
public class LevelEndUI : MonoBehaviour
{
    [SerializeField]
    Canvas _Canvas;
    [SerializeField]
    CanvasGroup _CanvasGroup;
    [SerializeField]
    Button _NextStageButton;
    [SerializeField]
    float _AnimationTime;

    public void Initialize()
    {
        _Canvas.gameObject.SetActive(false);
        _CanvasGroup.alpha = 0;
    }

    public async UniTask UIAnimation(CancellationToken token)
    {
        //次のステージ存在しない時次のステージへ進むボタン消す
        if (LevelProgressManager.Instance.IsExistNextStage() == false)
            _NextStageButton.gameObject.SetActive(false);

        _Canvas.gameObject.SetActive(true);
        _CanvasGroup.DOFade(1, _AnimationTime);
        await UniTask.Delay(TimeSpan.FromSeconds(_AnimationTime));
    }
}
