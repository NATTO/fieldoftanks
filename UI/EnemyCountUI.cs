using Cysharp.Text;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class EnemyCountUI : MonoBehaviour
{
    const string CountUITextBase = "�c��  {0} / {1}";
    [SerializeField]
    TextMeshProUGUI _Text;
    int _MaxCount;
    public void Initialize(int count){ _MaxCount = count; EnemyCountUpdate(_MaxCount); }
    public void EnemyCountUpdate(int count)
    {
        _Text.text = ZString.Format(CountUITextBase, count, _MaxCount);
    }
}
