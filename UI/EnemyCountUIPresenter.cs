using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class EnemyCountUIPresenter : MonoBehaviour
{
    [SerializeField]
    EnemyManager _EnemyManager;
    [SerializeField]
    EnemyCountUI _EnemyCountUI;

    private void Awake()
    {
        _EnemyCountUI.Initialize(_EnemyManager.EnemyCount.Value);
        _EnemyManager.EnemyCount.Subscribe(count => _EnemyCountUI.EnemyCountUpdate(count));
    }
}
