using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class StageSelectUIObject : MonoBehaviour
{
    [field: SerializeField]
    public TextMeshProUGUI _StageNumberText { get; private set; }

    [field: SerializeField]
    public Button _StageSelectButton { get; private set; }
}
