using Cysharp.Text;
using TMPro;
using UnityEngine;

/// <summary>
/// ステージ内で今がどこか書くやつ
/// </summary>
public class StageCount : MonoBehaviour
{
    const string _CountMessage = "いまのステージ　{0}ステージ目";
    [SerializeField]
    TextMeshProUGUI _Text;

    private void Awake()
    {
        _Text.text = ZString.Format(_CountMessage,SaveDataManager.LoadInt(SaveDataManager.LevelDataPath));
    }
}
