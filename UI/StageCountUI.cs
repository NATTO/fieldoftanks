using Cysharp.Text;
using TMPro;
using UnityEngine;

public class StageCountUI : MonoBehaviour
{
    const string CountUITextBase = "�X�e�[�W {0}";

    [SerializeField]
    TextMeshProUGUI _Text;

    public void StageCountUIUpdate(string stage)
    {
        _Text.text = ZString.Format(CountUITextBase, stage);
    }
}
