using System.Linq;
using UnityEngine;

/// <summary>
/// メニュー画面のステージ選択ボタン生成するやつ
/// </summary>
public class StageSelectUI : MonoBehaviour
{
    [SerializeField]
    StageSelectUIObject _UIObject;

    [SerializeField]
    Transform _UIParentTransform;


    private void Start()
    {
        int clearLevel = Mathf.Max(SaveDataManager.LoadInt(SaveDataManager.LevelDataPath),1);

            foreach (int i in Enumerable.Range(0, clearLevel))
            {
                int count = i;
                var uiButton = Instantiate(_UIObject, _UIParentTransform);
                uiButton._StageNumberText.text = (count + 1).ToString();
                uiButton._StageSelectButton.onClick.AddListener(() => LevelProgressManager.Instance.ProgressStart(count + LevelProgressManager._StageBuildIndex));
            }
    }

#if UNITY_EDITOR
    public void TestGenerate()
    {
        Start();
    }
#endif
}
