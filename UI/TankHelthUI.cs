using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;

public class TankHelthUI : MonoBehaviour, IStatusInitializable
{
    [SerializeField]
    Slider _HelthSlider;
    [SerializeField]
    TankHelth _Helth;
    [SerializeField]
    Vector3 _PositionOffset;

    Transform _Transform;
    RectTransform _HelthUITransform;

    public void Initialize(ScriptableTankStatus status)
    {
        _HelthSlider.maxValue = status._TankLife;

        _Helth.HelthReactiveProperty.Subscribe(helth => _HelthSlider.value = helth).AddTo(gameObject);
        _HelthUITransform = _HelthSlider.GetComponent<RectTransform>();
        _Transform = transform;
    }
}
