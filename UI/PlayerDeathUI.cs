﻿using UniRx;
using UnityEngine;
using Cysharp.Threading.Tasks;
using System;
using DG.Tweening;

/// <summary>
/// プレイヤー死んだときのUIアニメーション
/// </summary>
public class PlayerDeathUI : MonoBehaviour
{
    [SerializeField]
    RectTransform _UIParent;

    [SerializeField]
    float _FadeTime;

    [SerializeField]
    CanvasGroup _CanvasGroup;

    public void Initialize()
    {
        _UIParent.gameObject.SetActive(false);
        _CanvasGroup.alpha = 0f;
    }



    public async UniTask UIAnimation()
    {
        _UIParent.gameObject.SetActive(true);
        _CanvasGroup.DOFade(1, _FadeTime);
        await UniTask.Delay(TimeSpan.FromSeconds(_FadeTime));
    }
}