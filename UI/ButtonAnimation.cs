using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ButtonAnimation : MonoBehaviour
{
    [SerializeField]
    float _AnimTime;
    [SerializeField,Range(0,1)]
    float _StartSize;
    [SerializeField,Range(0.5f,1.5f)]
    float _EndSize;


}
