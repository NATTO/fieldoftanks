﻿/// <summary>
/// プレイヤーが必要な奴の初期化
/// </summary>
interface IPlayerObjectInitializable
{
    void Initialize(PlayerManager player);
}