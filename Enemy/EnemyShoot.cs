﻿using UnityEngine;

/// <summary>
/// 敵が撃つやつ
/// </summary>
public class EnemyShoot : TankShoot, IInfomationUpdatable
{
    protected float _BulletReloadTime;
    public bool IsShoot => _Status._BulletReloadTime < _BulletReloadTime;

    public void TimeReset()
    {
        _BulletReloadTime = 0;
    }

    protected override BulletMove BulletInstance()
    {
        var result = base.BulletInstance();
        result.SetIgnoreTag(TagNames.Enemy);
        return result;
    }

    public override void Shoot(Vector3 direction, string ignoreTag = "")
    {
        //生成して飛ばす
        var bulletObject = BulletInstance();
        if(ignoreTag != "")
        bulletObject.SetIgnoreTag(ignoreTag);
        bulletObject.MoveStart(direction);
    }

    public void InfomationUpdate()
    {        
        _BulletReloadTime += Time.deltaTime;
    }
}