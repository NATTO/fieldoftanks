using UnityEngine;

/// <summary>
/// プレイヤーを探す（撃つようのやつ）
/// </summary>
public class EnemySerch : MonoBehaviour, IInfomationUpdatable, IStatusInitializable,IPlayerObjectInitializable
{
    [SerializeField]
    PlayerManager _PlayerObject;
    [SerializeField]
    Transform _BullelTransform;

    Transform _Transform;
    Transform _PlayerTransform;

    bool _IsSpotted = false;
    public bool IsSpotted => _IsSpotted;
    /// <summary>
    /// プレイヤーへの方向（正規化されている）
    /// </summary>
    public Vector3 PlayerDirection => (_PlayerTransform.position - _Transform.position).normalized;

    public void Initialize(PlayerManager player)
    {
        _PlayerObject = player;
        _PlayerTransform = _PlayerObject.transform;
    }

    public void Initialize(ScriptableTankStatus status)
    {
        _Transform = transform;
    }

    public void InfomationUpdate()
    {        
        Vector3 direction = PlayerDirection;

        Ray ray = new Ray(_Transform.position, direction);

        //索敵
        if (Physics.Raycast(ray, out var infomation, Mathf.Infinity, LayerMasks.EnemyLayerIgnore))
        {
            _IsSpotted = infomation.collider.name == _PlayerObject.name;
        }
        //銃身をプレイヤーへ向ける
        _BullelTransform.forward = direction;
    }
}
