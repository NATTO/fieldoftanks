﻿using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// ルートに沿って動く敵の動き
/// </summary>
public class EnemyMoveRoute : MonoBehaviour, IEnemyMove, IStatusInitializable
{
    //動くルート
    [SerializeField]
    ScriptablePathTokenEnemy _EnemyRoute;
    [SerializeField]
    NavMeshAgent _Agent;
    [SerializeField]
    float _Speed;
    //今どこを目指してるか
    [SerializeField]
    int _RouteIndex;

    public void Initialize(ScriptableTankStatus status)
    {
        _Speed = status._TankSpeed;
        _Agent.speed = _Speed;
        _Agent.SetDestination(_EnemyRoute._OnTheWayPositions[_RouteIndex]._Position);
    }

    public void Move()
    {
        //次の目標に近づいたら次の目標設定
        if (_Agent.remainingDistance <= 1)
        {
            _RouteIndex++;
            if (_RouteIndex >= _EnemyRoute._OnTheWayPositions.Length)
                _RouteIndex = 0;
            
            _Agent.SetDestination(_EnemyRoute._OnTheWayPositions[_RouteIndex]._Position);
        }
    }
}