﻿using UnityEngine;


/// <summary>
/// Way系の弾を撃つ
/// </summary>
public class EnemyWayShoot : EnemyShoot
{
    //撃つ弾の数
    [SerializeField]
    int _BulletCount;
    //撃つ角度
    [SerializeField]
    float _ShootAngle;
    BulletMove[] _Bullets;

    public override void Initialize(ScriptableTankStatus status)
    {
        base.Initialize(status);
        _Bullets = new BulletMove[_BulletCount];
    }

    protected override BulletMove BulletInstance()
    {
        var result = base.BulletInstance();
        result.SetIgnoreTag(TagNames.Enemy);
        return result;
    }

    public override void Shoot(Vector3 direction, string ignoreTag = "")
    {
        for (int i = 0; i < _BulletCount; i++)
            _Bullets[i] = BulletInstance();

        int count = 0;
        Vector3 dir = direction;
        for (float angle = -_ShootAngle; angle < _ShootAngle; angle += _ShootAngle * 2 / _BulletCount)
        {
            var resultAngle = Quaternion.AngleAxis(angle, Vector3.up);
            var resultDirection = resultAngle * dir;
            _Bullets[count].SetIgnoreTag(ignoreTag);
            _Bullets[count].MoveStart(resultDirection.normalized);
            count++;
        }
    }
}