using System;
using TNRD;
using UnityEngine;

/// <summary>
/// エネミーの行動管理
/// </summary>
[RequireComponent(typeof(EnemySerch))]
[RequireComponent(typeof(EnemyShoot))]
public class EnemyController : MonoBehaviour
{
    [SerializeField]
    EnemySerch _Serch;
    [SerializeField]
    SerializableInterface<IEnemyMove> _Move;
    [SerializeField]
    EnemyShoot _Shoot;
    [SerializeField]
    TankHelth _Helth;
    [SerializeField]
    ScriptableTankStatus _TankStatus;
    [SerializeField]
    ScriptableEnemyTankStatus _EnemyTankStatus;

    IInfomationUpdatable[] _InfomationUpdatables;


    public void Initialize(PlayerManager player)
    {
        Array.ForEach(GetComponents<IPlayerObjectInitializable>(), (value) => value.Initialize(player));
        Array.ForEach(GetComponents<IStatusInitializable>(), (value) => value.Initialize(_TankStatus));
        Array.ForEach(GetComponents<IEnemyStatusInitializable>(), (value) => value.Initialize(_EnemyTankStatus));
        _InfomationUpdatables = GetComponents<IInfomationUpdatable>();
    }


    private void Update()
    {
        if (_Helth.HelthReactiveProperty.Value == 0) return;
        Array.ForEach(_InfomationUpdatables, value => value.InfomationUpdate());

        //撃てるなら撃つ
        if (_Serch.IsSpotted && _Shoot.IsShoot)
        {
            _Shoot.TimeReset();
            _Shoot.Shoot(_Serch.PlayerDirection);
        }
        //動けるなら動く
        if (_Move.Reference != null)
        {
            _Move.Value.Move();
        }
    }
}
