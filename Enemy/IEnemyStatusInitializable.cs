﻿
/// <summary>
/// 敵用のステータス初期化
/// </summary>
public interface IEnemyStatusInitializable
{
    void Initialize(ScriptableEnemyTankStatus status);
}