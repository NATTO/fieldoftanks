using UnityEngine;
using UnityEngine.AI;
using UniRx;
using Cysharp.Threading.Tasks;

/// <summary>
/// プレイヤーを追う敵の動き
/// </summary>
public class EnemyMoveChase : MonoBehaviour, IEnemyMove, IPlayerObjectInitializable, IStatusInitializable, IEnemyStatusInitializable
{
    //速度
    [SerializeField]
    float _MoveSpeed;
    //感知範囲
    [SerializeField]
    float _ChaseRadius;
    [SerializeField]
    NavMeshAgent _Agent;
    [SerializeField]
    TankHelth _Helth;

    Collider _Collider;
    PlayerManager _PlayerManager;
    Rigidbody _PlayerRigidbody;
    Rigidbody _Rigidbody;

    public void Initialize(PlayerManager player)
    {
        _PlayerManager = player;
        _PlayerRigidbody = player.GetComponent<Rigidbody>();
        _Collider = GetComponent<Collider>();
        _Rigidbody = GetComponent<Rigidbody>();        
    }

    public void Initialize(ScriptableTankStatus status)
    {
        _MoveSpeed = status._TankSpeed;
        _Agent.speed = _MoveSpeed;
    }

    public void Initialize(ScriptableEnemyTankStatus status)
    {
        _ChaseRadius = status._SerchRadius;
        _Helth.HelthReactiveProperty.Where(x => x == 0).First().Subscribe(_ =>
        {
            _Agent.updatePosition = false;
        }).AddTo(this);
    }

    public void Move()
    {
        Vector3 moveVector = (_PlayerRigidbody.position - transform.position).normalized;
        Ray toPlayerRay = new Ray(transform.position, moveVector);

        //感知範囲内でプレイヤーにRay飛ばす
        if (Physics.Raycast(toPlayerRay, out RaycastHit hit, _ChaseRadius, LayerMasks.EnemyLayerIgnore))
        {
            //感知できたなら追跡
            if (hit.collider.name == _PlayerManager.name)
            {
                _Agent.SetDestination(hit.point);
            }
        }
        _Rigidbody.velocity = Vector3.zero;
    }
}
