﻿using UnityEngine;

/// <summary>
/// 敵のステータス
/// </summary>
[CreateAssetMenu(menuName = "Status/EnemyTankStatus")]
public class ScriptableEnemyTankStatus : ScriptableObject
{
    public float _SerchRadius;
    public float _MoveTime;
    public float _StopTime;
}