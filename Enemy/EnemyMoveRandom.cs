using UnityEngine;

/// <summary>
/// 無作為に動く敵の動き
/// </summary>
[RequireComponent(typeof(TankMove))]
public class EnemyMoveRandom : MonoBehaviour, IStatusInitializable, IEnemyMove
{
    [SerializeField]
    TankMove _Move;

    [SerializeField]
    float _MoveTime;
    [SerializeField]
    float _StopTime;

    float _RantimeMoveTime;
    float _RantimeStopTime;

    [SerializeField, Range(-1f, 0)]
    float _MoveXMin = -1;
    [SerializeField, Range(0, 1f)]
    float _MoveXMax = 1;
    [SerializeField, Range(-1f, 0)]
    float _MoveYMin = -1;
    [SerializeField, Range(0, 1f)]
    float _MoveYMax = 1;

    enum MoveSwitch
    {
        Move,
        Stop
    }
    MoveSwitch _MoveSwitch = MoveSwitch.Move;
    [SerializeField]
    Vector2 _MoveVector;

    Vector2 RandomVector => new Vector2(Random.Range(_MoveXMin, _MoveXMax), Random.Range(_MoveYMin, _MoveYMax));

    public void Initialize(ScriptableTankStatus status)
    {
        _RantimeStopTime = _StopTime;
        _RantimeMoveTime = _MoveTime;
        _MoveVector = RandomVector;
    }

    public void Move()
    {
        switch (_MoveSwitch)
        {
            //ランダムな一方向に動く
            case MoveSwitch.Move:
                _RantimeMoveTime -= Time.deltaTime;
                _Move.Move(_MoveVector, LayerMasks.EnemyLayerIgnore);

                //動く時間が終わったなら止まる
                if (_RantimeMoveTime <= 0)
                {
                    _MoveSwitch = MoveSwitch.Stop;
                    _RantimeStopTime = _StopTime;
                }

                break;

            //止まる
            case MoveSwitch.Stop:
                _RantimeStopTime -= Time.deltaTime;
                //止まるのが終わったらランダムな方向を決める
                if (_RantimeStopTime <= 0)
                {
                    _MoveSwitch = MoveSwitch.Move;
                    _RantimeMoveTime = _MoveTime;
                    _MoveVector = RandomVector;
                }
                break;
        }
    }
}
