using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class EnemyCountBreakWall : MonoBehaviour
{
    [SerializeField]
    List<TankHelth> _Tanks;
    [SerializeField]
    List<GameObject> _Walls;
    int _Count = 0;

    private void Awake()
    {
        foreach(var tank in _Tanks)
        {
            tank.HelthReactiveProperty.Subscribe(helth =>
            {
                _Count++;
                if(_Count== _Tanks.Count)
                    foreach(var wall in _Walls) wall.SetActive(false);

            }).AddTo(tank);
        }
    }

}
