﻿using UnityEditor;
using UnityEngine;

[System.Serializable]
public class StageData : ScriptableObject
{
    public StageID[] _StageID;
}