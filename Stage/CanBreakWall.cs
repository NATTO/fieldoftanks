﻿using UnityEngine;

class CanBreakWall : MonoBehaviour, IDamagable
{
    [SerializeField]
    float _StartHelth;
    [SerializeField]
    Color _StartColor;
    [SerializeField]
    Color _EndColor;
    [SerializeField]
    Renderer _Renderer;
    [SerializeField]
    float _Helth;

    void Awake()
    {
        _Helth = _StartHelth;
    }
    
    void IDamagable.TakeDamage(float damage, Vector3 damagePosition)
    {
        _Helth -= damage;
        _Renderer.material.color = Color.Lerp(_StartColor, _EndColor,Mathf.Clamp01(1 -(_Helth / _StartHelth)));
        if(_Helth <= 0)
            Destroy(gameObject);
    }
}
