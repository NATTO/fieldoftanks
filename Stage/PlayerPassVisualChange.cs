using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPassVisualChange : MonoBehaviour
{
    [SerializeField]
    List<GameObject> _Walls;

    private void OnTriggerEnter(UnityEngine.Collider other)
    {
        if (other.CompareTag(TagNames.Player))
        {
            foreach (var i in _Walls) i?.SetActive(!i.activeSelf);
            Destroy(gameObject);
        }
    }
}
