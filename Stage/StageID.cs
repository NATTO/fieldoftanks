﻿

using UnityEngine;

[System.Serializable]
public struct StageID
{
    public string ID;
    public Sprite DescriptionSprite;
    public string SceneName;
}
